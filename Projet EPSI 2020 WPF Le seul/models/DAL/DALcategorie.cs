﻿using MySql.Data.MySqlClient;
using Projet_EPSI_2020_WPF_Le_seul.models.DAO;
using System;
using System.Collections.ObjectModel;
using System.Windows;


namespace Projet_EPSI_2020_WPF_Le_seul.models.DAL
{
    class DALcategorie
    {

        public static void AddCategorie(string nom)
        {
            string query = "INSERT INTO categories (nom) VALUES (\'" + nom + "') ";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch(Exception e)
            {
                MessageBox.Show("Il y a un problème dans la requête", e.StackTrace);
            }
        }

        public static DAOcategorie getCategorie(int id)
        {
            string query = "SELECT * FROM categories WHERE id=" + id + ";";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            DAOcategorie categorie = new DAOcategorie(reader.GetInt32(0), reader.GetString(1));
            reader.Close();
            return categorie;
        }

        public static void EditCategorie(int id, string nom)
        {
            string query = "UPDATE categories SET nom=\'" + nom + "' WHERE id=\'" + id + "'";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la requete : {0}", e.StackTrace);
            }
        }

        public static void RemoveCategorie(int id)
        {
            string query = "DELETE FROM categories WHERE id=\'" + id + "'";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la requete : {0}", e.StackTrace);
            }
        }

        public static ObservableCollection<DAOcategorie> getAllCategories()
        {
            ObservableCollection<DAOcategorie> categorieList = new ObservableCollection<DAOcategorie>();
            string query = "SELECT * FROM categories;";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    categorieList.Add(new DAOcategorie(reader.GetInt32(0), reader.GetString(1)));
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la table personne : {0}", e.StackTrace);
            }
            reader.Close();

            return categorieList;
        }

    }
}
