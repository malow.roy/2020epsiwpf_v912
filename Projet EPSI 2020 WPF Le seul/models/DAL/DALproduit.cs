﻿using MySql.Data.MySqlClient;
using Projet_EPSI_2020_WPF_Le_seul.models.DAO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;

namespace Projet_EPSI_2020_WPF_Le_seul.models.DAL
{
    class DALproduit
    {
        public static void AddProduit(string titre, string description, string volume, string price, string poids, string materiau, int idVendeur, int idCategorie,  int idLieu)
        {
            string query = "INSERT INTO produit (titre, description, volume, estimation, poids, matériau, IdVendeur, IdCategorie, IdLieu) VALUES (\'" + titre + "',\"" + description + "\",\"" + volume + "\",\"" + price + "\",\"" + poids + "\",\"" + materiau + "\",\"" + idVendeur + "\",\"" + idCategorie + "\",\"" + idLieu + "\") ";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la requête", e.StackTrace);
            }
        }

        public static ObservableCollection<DAOproduit> getAllProduits()
        {
            ObservableCollection<DAOproduit> produitList = new ObservableCollection<DAOproduit>();
            string query = "SELECT * FROM produit;";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    produitList.Add(new DAOproduit(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetString(6), reader.GetInt32(7), reader.GetInt32(8), reader.GetInt32(9), reader.GetInt32(10)));
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la table personne : {0}", e.StackTrace);
            }
            reader.Close();

            return produitList;
        }

        public static ObservableCollection<DAOproduit> getAllProduitsForLot()
        {
            ObservableCollection<DAOproduit> produitList = new ObservableCollection<DAOproduit>();
            string query = "SELECT * FROM produit WHERE IdLot IS NULL";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    produitList.Add(new DAOproduit(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetString(6), reader.GetInt32(7), reader.GetInt32(8), 0, reader.GetInt32(10)));
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.StackTrace, e.StackTrace);
            }
            reader.Close();

            return produitList;
        }

        public static ObservableCollection<DAOproduit> getAllProduitsFromLot(int id)
        {
            ObservableCollection<DAOproduit> produitList = new ObservableCollection<DAOproduit>();
            string query = "SELECT * FROM produit WHERE IdLot=\'" + id + "'";
            Console.WriteLine(query);
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            MySqlDataReader pflReader = null;
            try
            {
                cmd.ExecuteNonQuery();
                pflReader = cmd.ExecuteReader();

                while (pflReader.Read())
                {
                    produitList.Add(new DAOproduit(pflReader.GetInt32(0), pflReader.GetString(1), pflReader.GetString(2), pflReader.GetString(3), pflReader.GetString(4), pflReader.GetString(5), pflReader.GetString(6), pflReader.GetInt32(7), pflReader.GetInt32(8), pflReader.GetInt32(9), pflReader.GetInt32(10)));
                }

                pflReader.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.StackTrace, e.StackTrace);
            }
            return produitList;
        }

        public static void setProduitLot(int id, int lotId)
        {
            string query = "UPDATE produit SET IdLot=\'" + lotId + "' WHERE id=\'" + id + "'";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la requete du produit : {0}", e.StackTrace);
            }
        }

        public static void resetProduitLot(int id)
        {
            string query = "UPDATE produit SET IdLot = NULL WHERE IdLot=\'" + id + "'";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la requete du produit : {0}", e.StackTrace);
            }
        }
    }
}
