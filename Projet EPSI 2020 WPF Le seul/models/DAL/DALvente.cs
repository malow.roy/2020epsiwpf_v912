﻿using MySql.Data.MySqlClient;
using Projet_EPSI_2020_WPF_Le_seul.models.DAO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;

namespace Projet_EPSI_2020_WPF_Le_seul.models.DAL
{
    class DALvente
    {
        public static void AddVente(DateTime date, string titre, bool Isfinished, int idLieu)
        {
            int boolbool;
            if(Isfinished == true)
            {
                boolbool = 1;
            } else
            {
                boolbool = 0;
            }
            string sqlDate = date.ToString("yyyy-MM-dd HH:mm:ss");

            string query = "INSERT INTO vente (date, titre, IsFinished, IdLieu) VALUES (\'" + sqlDate + "',\"" + titre + "\",\"" + boolbool + "\",\"" + idLieu + "\") ";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.StackTrace, e.StackTrace);
            }
        }

        public static ObservableCollection<DAOvente> getAllVentes()
        {
            string isFinished;
            ObservableCollection<DAOvente> venteList = new ObservableCollection<DAOvente>();
            string query = "SELECT * FROM vente;";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    if(reader.GetInt32(3) == 0)
                    {
                        isFinished = "En attente";
                    }
                    else
                    {
                        isFinished = "Terminée";
                    }

                    venteList.Add(new DAOvente(reader.GetInt32(0), reader.GetDateTime(1), reader.GetString(2), isFinished, reader.GetInt32(4)));
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la table personne : {0}", e.StackTrace);
            }
            reader.Close();

            return venteList;
        }

        public static DAOvente getVente(int id)
        {
            string isFinished;
            DAOvente newVente = new DAOvente(0, DateTime.Now, null, null, 0);
            string query = "SELECT * FROM vente WHERE id=" + id + ";";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                if (reader.GetInt32(3) == 0)
                {
                    isFinished = "En attente";
                }
                else
                {
                    isFinished = "Terminée";
                }
                newVente = new DAOvente(reader.GetInt32(0), (DateTime)reader.GetDateTime(1), reader.GetString(2), isFinished, reader.GetInt32(4));
            }
            reader.Close();
            return newVente;
        }


        public static void RemoveVente(int id)
        {
            string query = "DELETE FROM vente WHERE id=\'" + id + "'";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la requete : {0}", e.StackTrace);
            }
        }

        public static void EditVente(int id, DateTime date, string titre, bool Isfinished, int lieuId)
        {
            int boolbool;
            if (Isfinished == true)
            {
                boolbool = 1;
            }
            else
            {
                boolbool = 0;
            }
            string sqlDate = date.ToString("yyyy-MM-dd HH:mm:ss");

            string query = "UPDATE vente SET date=\'" + sqlDate + "', ville=\'" + titre + "', IsFinished=\'" + boolbool + "', IdLieu=\'" + lieuId + "' WHERE id=\'" + id + "'";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la requete : {0}", e.StackTrace);
            }
        }



    }
}
