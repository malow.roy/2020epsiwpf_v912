﻿using MySql.Data.MySqlClient;
using Projet_EPSI_2020_WPF_Le_seul.models.DAO;
using Projet_EPSI_2020_WPF_Le_seul.views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;

namespace Projet_EPSI_2020_WPF_Le_seul.models.DAL
{
    class DALlieu
    {
        public static ObservableCollection<DAOlieu> getAllLieux()
        {
            ObservableCollection<DAOlieu> locationList = new ObservableCollection<DAOlieu>();
            string query = "SELECT * FROM lieu;";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    locationList.Add(new DAOlieu(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4)));
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la table personne : {0}", e.StackTrace);
            }
            reader.Close();

            return locationList;
        }

        public static DAOlieu getLieu(int id)
        {
            string query = "SELECT * FROM lieu WHERE id=" + id + ";";
            DAOlieu location = new DAOlieu(0, null, null, null, null);
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                location = new DAOlieu(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4));
            }
            reader.Close();
            return location;
        }

        public static void AddLieu(string nom, string ville, string adresse, string postalcode)
        {
            string query = "INSERT INTO lieu (nom, ville, adresse, postalcode) VALUES (\'" + nom + "', \'" + ville + "' , \'" + adresse + "', \'" + postalcode + "' )";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la requete : {0}", e.StackTrace);
            }
        }

        public static void RemoveLieu(int id)
        {
            string query = "DELETE FROM lieu WHERE id=\'" + id + "'";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la requete : {0}", e.StackTrace);
            }
        }

        public static void EditLieu(int id, string nom, string ville, string adresse, string postalcode)
        {
            string query = "UPDATE lieu SET nom=\'" + nom + "', ville=\'" + ville + "', adresse=\'" + adresse + "', postalcode=\'" + postalcode + "' WHERE id=\'" + id + "'";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la requete : {0}", e.StackTrace);
            }
        }

    }
}
