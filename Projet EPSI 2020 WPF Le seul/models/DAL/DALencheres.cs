﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Projet_EPSI_2020_WPF_Le_seul.models.DAO;

namespace Projet_EPSI_2020_WPF_Le_seul.models.DAL
{
    public class DALencheres
    {
        public static ObservableCollection<DAOencheres> getAllEncheres()
        {
            ObservableCollection<DAOencheres> enchereList = new ObservableCollection<DAOencheres>();
            string query = "SELECT * FROM enchere;";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    enchereList.Add(new DAOencheres(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2), reader.GetInt32(3), reader.GetInt32(4)));
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la table personne : {0}", e.StackTrace);
            }
            reader.Close();

            return enchereList;
        }

        public static DAOencheres getEnchere(int id)
        {
            string query = "SELECT * FROM enchere WHERE id=" + id + ";";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            DAOencheres enchere = new DAOencheres(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2), reader.GetInt32(3), reader.GetInt32(4));
            reader.Close();
            return enchere;
        }

        public static void AddEnchere(string prix, int idVente, int idAcheteur, int idLot)
        {
            string query = "INSERT INTO enchere (prix, IdVente, IdAcheteur, IdLot) VALUES (\'" + prix + "', \'" + idVente + "' , \'" + idAcheteur + "', \'" + idLot + "' )";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la requete: {0}", e.StackTrace);
            }
        }

        public static void RemoveEnchere(int id)
        {
            string query = "DELETE FROM enchere WHERE id=\'" + id + "'";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la requete : {0}", e.StackTrace);
            }
        }

        public static void EditEnchere(int id, string price, int idVente, int idAcheteur, int idProduit)
        {
            string query = "UPDATE enchere SET price=\'" + price + "', IdVente=\'" + idVente + "', IdAcheteur=\'" + idAcheteur + "', IdProduit=\'" + idProduit + "' WHERE id=\'" + id + "'";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la requete : {0}", e.StackTrace);
            }
        }


    }
}
