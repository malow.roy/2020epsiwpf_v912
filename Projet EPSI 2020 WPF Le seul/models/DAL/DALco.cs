﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MySql.Data.MySqlClient;
using Projet_EPSI_2020_WPF_Le_seul.models.DAO;

namespace Projet_EPSI_2020_WPF_Le_seul.models.DAL
{
    public class DALco
    {
        public static bool connection(string email, string mdp)
        {
            bool co = false;
            string query = "SELECT * FROM user WHERE email=\"" + email + "\" and mdp=\"" + mdp + "\";";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        if (reader.GetInt32(0) != 0)
                        {
                            co = true;
                        }
                    }
                    reader.Close();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

            return co;
        }


        public static ObservableCollection<DAOco> getAllCos()
        {
            ObservableCollection<DAOco> coList = new ObservableCollection<DAOco>();
            string query = "SELECT * FROM user;";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    coList.Add(new DAOco(reader.GetInt32(0), reader.GetString(2)));
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la table personne : {0}", e.StackTrace);
            }
            reader.Close();

            return coList;
        }
    }
}
