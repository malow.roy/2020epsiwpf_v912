﻿using MySql.Data.MySqlClient;
using Projet_EPSI_2020_WPF_Le_seul.models.DAO;
using Projet_EPSI_2020_WPF_Le_seul.viewmodels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Text;
using System.Windows;

namespace Projet_EPSI_2020_WPF_Le_seul.models.DAL
{
    public class DALlot
    {
        public static ObservableCollection<DAOlot> getAllLotsFromAnEvent(int idVente)
        {
            ObservableCollection<DAOlot> lotList = new ObservableCollection<DAOlot>();
            string query = "SELECT * FROM lot WHERE idVente=" + idVente + ";";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    lotList.Add(new DAOlot(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(3)));
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la table personne : {0}", e.StackTrace);
            }
            reader.Close();

            return lotList;
        }

        public static ObservableCollection<DAOlot> getAllLots()
        {
            ObservableCollection<DAOlot> lotList = new ObservableCollection<DAOlot>();
            string query = "SELECT * FROM lot";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            MySqlDataReader reader = null;
            try
            {
                cmd.ExecuteNonQuery();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    if (!reader.IsDBNull(3))
                    {
                        lotList.Add(new DAOlot(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(3)));
                    }
                    else
                    {
                        lotList.Add(new DAOlot(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), 0));
                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.StackTrace, e.StackTrace);
            }
            reader.Close();
            return lotList;
        }

        public static void addLot(string name, string description, ObservableCollection<ViewModelProduit> produits)
        {
            string query = "INSERT INTO lot (nom, description) VALUES (\'" + name + "', \'" + description + "')";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            try
            {
                cmd.ExecuteNonQuery();
                int newId = DALlot.getLastInsertedId();
                foreach (ViewModelProduit produit in produits)
                {
                    DALproduit.setProduitLot(produit.Id, newId);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la requete : {0}", e.StackTrace);
            }
        }

        public static void editLot(int id, string name, string description, ObservableCollection<ViewModelProduit> produits)
        {
            string query = "UPDATE lot SET nom=\'" + name + "', description=\'" + description + "' WHERE id=\'" + id + "'";
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            try
            {
                cmd.ExecuteNonQuery();
                DALproduit.resetProduitLot(id);

                foreach (ViewModelProduit produit in produits)
                {
                    DALproduit.setProduitLot(produit.Id, id);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la requete : {0}", e.StackTrace);
            }
        }

        public static DAOlot getLot(int id)
        {
            string query = "SELECT * FROM lot WHERE id=" + id + ";";
            DAOlot lot = new DAOlot(0, null, null, 0);
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            if (!reader.IsDBNull(3))
            {
                lot = new DAOlot(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(3));
            }
            else
            {
               lot = new DAOlot(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), 0);
            }
            reader.Close();
            return lot;
        }

        public static int getLastInsertedId()
        {
            string getIdQuery = "SELECT max(id) FROM lot;";
            MySqlCommand getIdCmd = new MySqlCommand(getIdQuery, DALConnection.Open());
            getIdCmd.ExecuteNonQuery();
            MySqlDataReader reader = getIdCmd.ExecuteReader();
            reader.Read();
            int id = reader.GetInt32(0);
            reader.Close();
            return id;
        }

        public static void RemoveLot(int id)
        {
            DALproduit.resetProduitLot(id);
            string query = "DELETE FROM lot WHERE id=\'" + id + "'";
            Console.WriteLine(query);
            MySqlCommand cmd = new MySqlCommand(query, DALConnection.Open());
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show("Il y a un problème dans la requete : {0}", e.StackTrace);
            }
        }

    }
}
