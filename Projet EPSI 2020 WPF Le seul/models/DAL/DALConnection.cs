﻿using MySql.Data.MySqlClient;
using System;

namespace Projet_EPSI_2020_WPF_Le_seul.models.DAL
{
    class DALConnection
    {
        private static string server;
        private static string database;
        private static string uid;
        private static string password;
        public static MySqlConnection connection;

        public static MySqlConnection Open()
        {
            if (connection == null) 
            {
                server = "localhost";
                database = "projetc";
                uid = "projetc";
                password = "projet";
                string connectionString;
                connectionString = "SERVER=" + server + ";" + "DATABASE=" +
                database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
                connection = new MySqlConnection(connectionString);
                connection.Open();
            }
            return connection;
        }
    }
}
