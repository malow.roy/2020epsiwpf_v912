﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projet_EPSI_2020_WPF_Le_seul.models.DAO;
using Projet_EPSI_2020_WPF_Le_seul.viewmodels;

namespace Projet_EPSI_2020_WPF_Le_seul.models.ORM
{
    class ORMencheres
    {
        public static ObservableCollection<ViewModelEncheres> getAllEncheres()
        {
            ObservableCollection<DAOencheres> lDAO = DAOencheres.getAllEncheres();
            ObservableCollection<ViewModelEncheres> enchereVMList = new ObservableCollection<ViewModelEncheres>();
            foreach (DAOencheres element in lDAO)
            {
                ViewModelEncheres enchereVM = new ViewModelEncheres(element.id, element.price, element.idVente, element.idAcheteur, element.idLot);
                enchereVMList.Add(enchereVM);
            }
            return enchereVMList;

        }

        public static void RemoveEnchere(int id)
        {
            DAOencheres.RemoveEnchere(id);
        }

        public static void EditEnchere(int id, string price, int idVente, int idAcheteur, int idLot)
        {
            DAOencheres.EditEnchere(id, price, idVente, idAcheteur, idLot);
        }

        public static ViewModelEncheres getEnchere(int id)
        {
            DAOencheres daoEnchere = DAOencheres.GetEnchere(id);
            ViewModelEncheres enchereVM = new ViewModelEncheres(daoEnchere.id, daoEnchere.price, daoEnchere.idVente, daoEnchere.idAcheteur, daoEnchere.idLot);
            return enchereVM;
        }

        public static void AddEnchere(string price, int idVente, int idAcheteur, int idLot)
        {
            DAOencheres.AddEnchere(price, idVente, idAcheteur, idLot);
        }

    }
}
