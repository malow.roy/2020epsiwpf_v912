﻿using Projet_EPSI_2020_WPF_Le_seul.models.DAO;
using Projet_EPSI_2020_WPF_Le_seul.viewmodels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Projet_EPSI_2020_WPF_Le_seul.models.ORM
{
    class ORMcategorie
    {
        public static ObservableCollection<ViewModelCategorie> getAllCategories() {

            ObservableCollection<DAOcategorie> cDAO = DAOcategorie.getAllCategories();
            ObservableCollection<ViewModelCategorie> categorieVMList = new ObservableCollection<ViewModelCategorie>();
            foreach (DAOcategorie element in cDAO)
            {
                ViewModelCategorie categorieVM = new ViewModelCategorie(element.id, element.nom);
                categorieVMList.Add(categorieVM);
            }
            return categorieVMList;
        }

        public static void RemoveCategorie(int id)
        {
            DAOcategorie.RemoveCategorie(id);
        }

        public static void EditCategorie(int id, string name)
        {
            DAOcategorie.EditCategorie(id, name);
        }

        public static ViewModelCategorie getCategorie(int id)
        {
            DAOcategorie daoCateg = DAOcategorie.GetCategorie(id);
            ViewModelCategorie categVM = new ViewModelCategorie(daoCateg.id, daoCateg.nom);
            return categVM;
        }

        public static void AddCategorie(string nom)
        {
            DAOcategorie.AddCategorie(nom);
        }
    }
}
