﻿using Projet_EPSI_2020_WPF_Le_seul.models.DAO;
using Projet_EPSI_2020_WPF_Le_seul.viewmodels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Projet_EPSI_2020_WPF_Le_seul.models.ORM
{
    class ORMlieu
    {
        public static ObservableCollection<LieuViewModel> getAllLieux()
        {
            ObservableCollection<DAOlieu> lDAO = DAOlieu.getAllLieux();
            ObservableCollection<LieuViewModel> lieuVMList = new ObservableCollection<LieuViewModel>();
            foreach (DAOlieu element in lDAO)
            {
                LieuViewModel lieuVM = new LieuViewModel(element.id, element.name, element.city, element.address, element.postalcode);
                lieuVMList.Add(lieuVM);
            }
            return lieuVMList;

        }


        public static void RemoveLieu(int id)
        {
            DAOlieu.RemoveLieu(id);
        }

        public static void EditLieu(int id, string name, string city, string address, string postalcode)
        {
            DAOlieu.EditLieu(id, name, city, address, postalcode);
        }

        public static LieuViewModel getLieu(int id)
        {
            DAOlieu daoLieu = DAOlieu.GetLieu(id);
            LieuViewModel lieuVM = new LieuViewModel(daoLieu.id, daoLieu.name, daoLieu.city, daoLieu.address, daoLieu.postalcode);
            return lieuVM;
        }

        public static void AddLieu(string name, string city, string address, string postalcode)
        {
            DAOlieu.AddLieu(name, city, address, postalcode);
        } 


    }
}
