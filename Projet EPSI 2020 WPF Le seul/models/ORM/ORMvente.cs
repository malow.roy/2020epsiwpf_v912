﻿using Projet_EPSI_2020_WPF_Le_seul.models.DAO;
using Projet_EPSI_2020_WPF_Le_seul.viewmodels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Projet_EPSI_2020_WPF_Le_seul.models.ORM
{
    class ORMvente
    {
        public static ObservableCollection<ViewModelVente> getAllVentes()
        {

            ObservableCollection<DAOvente> vDAO = DAOvente.getAllVentes();
            ObservableCollection<ViewModelVente> venteVMList = new ObservableCollection<ViewModelVente>();
            foreach (DAOvente element in vDAO)
            {
                LieuViewModel lieu = ORMlieu.getLieu(element.lieuId);
                ViewModelVente venteVM = new ViewModelVente(element.id, element.titre, element.date, element.Isfinished, lieu);
                venteVMList.Add(venteVM);
            }
            return venteVMList;
        }

        public static void RemoveVente(int id)
        {
            DAOvente.RemoveVente(id);
        }

        public static ViewModelVente getVente(int eventId)
        {
            DAOvente daoVente = DAOvente.GetVente(eventId);
            LieuViewModel lieu = ORMlieu.getLieu(daoVente.lieuId);
            ViewModelVente venteVM = new ViewModelVente(daoVente.id, daoVente.titre, daoVente.date, daoVente.Isfinished, lieu);
            return venteVM;
        }

        public static void AddVente(DateTime date, string titre, bool Isfinished, int idLieu)
        {
            DAOvente.AddVente(date, titre, Isfinished, idLieu);
        }

        public static void EditVente(int id, DateTime date, string titre, bool Isfinished, int idLieu)
        {
            DAOvente.EditVente(id, date, titre, Isfinished, idLieu);
        }
    }
}
