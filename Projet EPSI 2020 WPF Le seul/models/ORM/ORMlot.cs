﻿using Projet_EPSI_2020_WPF_Le_seul.models.DAO;
using Projet_EPSI_2020_WPF_Le_seul.viewmodels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Projet_EPSI_2020_WPF_Le_seul.models.ORM
{
    class ORMlot
    {
        public static ObservableCollection<ViewModelLot> getAllLotsFromAnEvent(int idEvent)
        {
            ObservableCollection<DAOlot> lDAO = DAOlot.getAllLotsFromAnEvent(idEvent);
            ObservableCollection<ViewModelLot> lotVMList = new ObservableCollection<ViewModelLot>();
            foreach (DAOlot element in lDAO)
            {
                ViewModelVente venteVM = ORMvente.getVente(element.eventId);
                ViewModelLot lotVM = new ViewModelLot(element.id, element.name, element.description, new ViewModelVente(0, "Pas de vente", DateTime.Now, null, new LieuViewModel(0, null, null, null, null)));

                if (venteVM != null)
                {
                    lotVM = new ViewModelLot(element.id, element.name, element.description, venteVM);
                }
                lotVMList.Add(lotVM);
            }
            return lotVMList;
        }

        public static ObservableCollection<ViewModelLot> getAllLots()
        {
            ObservableCollection<DAOlot> lDAO = DAOlot.getAllLots();
            ObservableCollection<ViewModelLot> lotVMList = new ObservableCollection<ViewModelLot>();
            foreach (DAOlot element in lDAO)
            {
                ViewModelLot lotVM = new ViewModelLot(element.id, element.name, element.description, new ViewModelVente(0, "N'appartient pas à une vente", DateTime.Now, null, new LieuViewModel(0, null, null, null, null)));

                if (element.eventId != 0)
                {
                    ViewModelVente venteVM = ORMvente.getVente(element.eventId);
                    lotVM = new ViewModelLot(element.id, element.name, element.description, venteVM);
                }

                lotVMList.Add(lotVM);
            }
            return lotVMList;
        }

        public static void AddLot(string name, string description, ObservableCollection<ViewModelProduit> produits) 
        {
            DAOlot.addLot(name, description, produits);
        }

        public static void EditLot(int id, string name, string description, ObservableCollection<ViewModelProduit> produits)
        {
            DAOlot.EditLot(id, name, description, produits);
        }

        public static void RemoveLot(int id)
        {
            DAOlot.removeLot(id);
        }

        public static ViewModelLot getLot(int id)
        {
            DAOlot daoLot = DAOlot.getLot(id);
            ViewModelVente newVente = ORMvente.getVente(daoLot.eventId);
            ViewModelLot lotVM = new ViewModelLot(daoLot.id, daoLot.name, daoLot.description, newVente);
            return lotVM;
        }

        public static ObservableCollection<ViewModelLot> getAllLots()
        {
            ObservableCollection<DAOlot> lDAO = DAOlot.getAllLots();
            ObservableCollection<ViewModelLot> lotVMLists = new ObservableCollection<ViewModelLot>();
            foreach (DAOlot element in lDAO)
            {
                ViewModelVente ventesVM = ORMvente.getVente(element.eventId);
                ViewModelLot lotVMs = new ViewModelLot(element.id, element.name, element.description, ventesVM);
                lotVMLists.Add(lotVMs);
            }
            return lotVMLists;

        }

    }
}
