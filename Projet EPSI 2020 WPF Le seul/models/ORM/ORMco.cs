﻿using Projet_EPSI_2020_WPF_Le_seul.models.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projet_EPSI_2020_WPF_Le_seul.models.DAO;
using System.Collections.ObjectModel;
using Projet_EPSI_2020_WPF_Le_seul.viewmodels;

namespace Projet_EPSI_2020_WPF_Le_seul.models.ORM
{
    class ORMco
    {
        public static bool connection(string email, string mdp)
        {
            return DAOco.connection(email, mdp);
        }


        public static ObservableCollection<ViewModelCo> getAllCos()
        {
            ObservableCollection<DAOco> cDAO = DAOco.getAllCos();
            ObservableCollection<ViewModelCo> coVMList = new ObservableCollection<ViewModelCo>();
            foreach (DAOco element in cDAO)
            {
                ViewModelCo coVM = new ViewModelCo(element.id, element.nom);
                coVMList.Add(coVM);
            }
            return coVMList;

        }

    }
}
