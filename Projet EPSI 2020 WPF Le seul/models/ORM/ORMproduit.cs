﻿using Projet_EPSI_2020_WPF_Le_seul.models.DAO;
using Projet_EPSI_2020_WPF_Le_seul.viewmodels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Projet_EPSI_2020_WPF_Le_seul.models.ORM
{
    class ORMproduit
    {
        public static ObservableCollection<ViewModelProduit> getAllProduits()
        {

            ObservableCollection<DAOproduit> pDAO = DAOproduit.getAllProduits();
            ObservableCollection<ViewModelProduit> produitVMList = new ObservableCollection<ViewModelProduit>();
            foreach (DAOproduit element in pDAO)
            {
                ViewModelProduit produitVM = new ViewModelProduit(element.id, element.titre, element.description, element.volume, element.price, element.poids, element.materiau, element.idVendeur, element.idCategorie, element.idLot, element.idLieu);
               produitVMList.Add(produitVM);
            }
            return produitVMList;
        }

        public static ObservableCollection<ViewModelProduit> getAllProduitsForLot()
        {

            ObservableCollection<DAOproduit> pDAO = DAOproduit.getAllProduitsForLot();
            ObservableCollection<ViewModelProduit> produitVMList = new ObservableCollection<ViewModelProduit>();
            foreach (DAOproduit element in pDAO)
            {
                ViewModelProduit produitVM = new ViewModelProduit(element.id, element.titre, element.description, element.volume, element.price, element.poids, element.materiau, element.idVendeur, element.idCategorie, element.idLot, element.idLieu);
                produitVMList.Add(produitVM);
            }
            return produitVMList;
        }

        public static ObservableCollection<ViewModelProduit> getAllProduitsFromLot(int id)
        {

            ObservableCollection<DAOproduit> pDAO = DAOproduit.getAllProduitsFromLot(id);
            ObservableCollection<ViewModelProduit> produitVMList = new ObservableCollection<ViewModelProduit>();
            foreach (DAOproduit element in pDAO)
            {
                ViewModelProduit produitVM = new ViewModelProduit(element.id, element.titre, element.description, element.volume, element.price, element.poids, element.materiau, element.idVendeur, element.idCategorie, element.idLot, element.idLieu);
                produitVMList.Add(produitVM);
            }
            return produitVMList;
        }

        public static void AddProduit(string titre, string description, string volume, string price, string poids, string materiau, int idVendeur, int idCategorie, int idLieu)
        {
            DAOproduit.AddProduit(titre, description, volume, price, poids, materiau, idVendeur, idCategorie, idLieu);
        }

    }
}
