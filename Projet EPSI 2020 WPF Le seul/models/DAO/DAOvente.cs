﻿using Projet_EPSI_2020_WPF_Le_seul.models.DAL;
using Projet_EPSI_2020_WPF_Le_seul.viewmodels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Projet_EPSI_2020_WPF_Le_seul.models.DAO
{
    class DAOvente
    {
        public int id;
        public DateTime date;
        public string titre;
        public string Isfinished;
        public int lieuId;

        public DAOvente(int id, DateTime date, string titre, string Isfinished, int lieuId)
        {
            this.id = id;
            this.date = date;
            this.titre = titre;
            this.Isfinished = Isfinished;
            this.lieuId = lieuId;
        }

        public static ObservableCollection<DAOvente> getAllVentes()
        {
            ObservableCollection<DAOvente> ca = DALvente.getAllVentes();
            return ca;
        }

        public static void AddVente(DateTime date, string titre, bool Isfinished, int lieuId)
        {
            DALvente.AddVente(date, titre, Isfinished, lieuId);
        }

        public static void EditVente(int id, DateTime date, string titre, bool Isfinished, int lieuId)
        {
            DALvente.EditVente(id, date, titre, Isfinished, lieuId);
        }

        public static void RemoveVente(int id)
        {
            DALvente.RemoveVente(id);
        }

        public static DAOvente GetVente(int id)
        {
            DAOvente e = DALvente.getVente(id);
            return e;
        }
    }
}
