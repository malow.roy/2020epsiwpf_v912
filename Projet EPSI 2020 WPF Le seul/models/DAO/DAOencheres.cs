﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projet_EPSI_2020_WPF_Le_seul.models.DAL;

namespace Projet_EPSI_2020_WPF_Le_seul.models.DAO
{
    public class DAOencheres
    {
        public int id;
        public string price;
        public int idVente;
        public int idAcheteur;
        public int idLot; 

        public DAOencheres(int id, string price, int idVente, int idAcheteur, int idLot)
        {
            this.id = id;
            this.price = price;
            this.idVente = idVente;
            this.idAcheteur = idAcheteur;
            this.idLot = idLot;
        }

        public static ObservableCollection<DAOencheres> getAllEncheres()
        {
            ObservableCollection<DAOencheres> enchereList = DALencheres.getAllEncheres();
            return enchereList;
        }


        public static DAOencheres GetEnchere(int id)
        {
            DAOencheres enchere = DALencheres.getEnchere(id);
            return enchere;
        }

        public static void EditEnchere(int id, string price, int idVente, int idAcheteur, int idLot)
        {
            DALencheres.EditEnchere(id, price, idVente, idAcheteur, idLot);
        }

        public static void AddEnchere(string price, int idVente, int idAcheteur, int idLot)
        {
            DALencheres.AddEnchere(price, idVente, idAcheteur, idLot);
        }

        public static void RemoveEnchere(int id)
        {
            DALencheres.RemoveEnchere(id);
        }




    }
}
