﻿using Projet_EPSI_2020_WPF_Le_seul.models.DAL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Projet_EPSI_2020_WPF_Le_seul.models.DAO
{
    class DAOcategorie
    {
        public int id;
        public string nom;


        public DAOcategorie(int id, string nom)
        {
            this.id = id;
            this.nom = nom;
        }

        public static ObservableCollection<DAOcategorie> getAllCategories()
        {
            ObservableCollection<DAOcategorie> categorieList = DALcategorie.getAllCategories();
            return categorieList;
        }

        public static DAOcategorie GetCategorie(int id)
        {
            DAOcategorie categorie = DALcategorie.getCategorie(id);
            return categorie;
        }

        public static void EditCategorie(int id, string name)
        {
            DALcategorie.EditCategorie(id, name);
        }

        public static void AddCategorie(string nom)
        {
            DALcategorie.AddCategorie(nom);
        }

        public static void RemoveCategorie(int id)
        {
            DALcategorie.RemoveCategorie(id);
        }
    
     
    }
}
