﻿using Projet_EPSI_2020_WPF_Le_seul.models.DAL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Projet_EPSI_2020_WPF_Le_seul.models.DAO
{
    public class DAOlieu
    {
        public int id;
        public string name;
        public string city;
        public string address;
        public string postalcode;

        public DAOlieu(int id, string name, string city, string address, string postalcode)
        {
            this.id = id;
            this.name = name;
            this.city = city;
            this.address = address;
            this.postalcode = postalcode;
        }

        public static ObservableCollection<DAOlieu> getAllLieux()
        {
            ObservableCollection<DAOlieu> lieuList = DALlieu.getAllLieux();
            return lieuList;
        }

        public static DAOlieu GetLieu(int id)
        {
            DAOlieu lieu = DALlieu.getLieu(id);
            return lieu;
        }

        public static void EditLieu(int id, string name, string city, string address, string postalcode)
        {
            DALlieu.EditLieu(id, name, city, address, postalcode);
        }

        public static void AddLieu(string name, string city, string address, string postalcode)
        {
            DALlieu.AddLieu(name, city, address, postalcode);
        }

        public static void RemoveLieu(int id)
        {
            DALlieu.RemoveLieu(id);
        }

    }

}
