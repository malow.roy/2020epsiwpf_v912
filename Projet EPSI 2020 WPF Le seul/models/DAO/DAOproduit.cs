﻿using Projet_EPSI_2020_WPF_Le_seul.models.DAL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Projet_EPSI_2020_WPF_Le_seul.models.DAO
{
    class DAOproduit
    {
        public int id;
        public string titre;
        public string description;
        public string volume;
        public string price;
        public string poids;
        public string materiau;
        public int idVendeur;
        public int idCategorie;
        public int idLot;
        public int idLieu;



        public DAOproduit(int id, string titre, string description, string volume, string price, string poids, string materiau, int idVendeur, int idCategorie, int idLot, int idLieu )
        {
            this.id = id;
            this.titre = titre;
            this.description = description;
            this.volume = volume;
            this.price = price;
            this.poids = poids;
            this.materiau = materiau;
            this.idVendeur = idVendeur;
            this.idCategorie = idCategorie;
            this.idLot = idLot;
            this.idLieu = idLieu;

        }

        public static ObservableCollection<DAOproduit> getAllProduits()
        {
            ObservableCollection<DAOproduit> ca = DALproduit.getAllProduits();
            return ca;
        }

        public static ObservableCollection<DAOproduit> getAllProduitsForLot()
        {
            ObservableCollection<DAOproduit> ca = DALproduit.getAllProduitsForLot();
            return ca;
        }

        public static ObservableCollection<DAOproduit> getAllProduitsFromLot(int id)
        {
            ObservableCollection<DAOproduit> ca = DALproduit.getAllProduitsFromLot(id);
            return ca;
        }

        public static void AddProduit(string titre, string description, string volume, string price, string poids, string materiau, int idVendeur, int idCategorie, int idLieu)
        {
            DALproduit.AddProduit(titre, description, volume, price, poids, materiau, idVendeur, idCategorie, idLieu);
        }

    }
}
