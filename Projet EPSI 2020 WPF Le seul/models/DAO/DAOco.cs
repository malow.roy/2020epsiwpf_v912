﻿using Projet_EPSI_2020_WPF_Le_seul.models.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projet_EPSI_2020_WPF_Le_seul.models.DAL;
using System.Collections.ObjectModel;

namespace Projet_EPSI_2020_WPF_Le_seul.models.DAO
{
    public class DAOco
    {
        public int id;
        public string email;
        public string mdp;
        public string nom;

        public DAOco(int id, string nom)
        {
            this.id = id;
            this.nom = nom;

        }


        public DAOco(string email, string mdp)
        {
           
            this.email = email;
            this.mdp = mdp;
        }


        public static bool connection(string email, string mdp)
        {
            return DALco.connection(email, mdp);
        }

        public static ObservableCollection<DAOco> getAllCos()
        {
            ObservableCollection<DAOco> coList = DALco.getAllCos();
            return coList;
        }


    }
}
