﻿using MySql.Data.MySqlClient;
using Projet_EPSI_2020_WPF_Le_seul.models.DAL;
using Projet_EPSI_2020_WPF_Le_seul.viewmodels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Projet_EPSI_2020_WPF_Le_seul.models.DAO
{
    public class DAOlot
    {
        public int id;
        public string name;
        public string description;
        public int eventId;

        public DAOlot(int id, string name, string description, int eventId)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.eventId = eventId;
        }

        public static void addLot(string name, string description, ObservableCollection<ViewModelProduit> produits)
        {
            DALlot.addLot(name, description, produits);
        }

        public static void EditLot(int id, string name, string description, ObservableCollection<ViewModelProduit> produits)
        {
            DALlot.editLot(id, name, description, produits);
        }

        public static void removeLot(int id)
        {
            DALlot.RemoveLot(id);
        }

        public static ObservableCollection<DAOlot> getAllLotsFromAnEvent(int idEvent)
        {
            ObservableCollection<DAOlot> lotList = DALlot.getAllLotsFromAnEvent(idEvent);
            return lotList;
        }

        public static ObservableCollection<DAOlot> getAllLots()
        {
            ObservableCollection<DAOlot> lotList = DALlot.getAllLots();
            return lotList;
        }

        public static DAOlot getLot(int id)
        {
            DAOlot lot = DALlot.getLot(id);
            return lot;
        }

    }

}
