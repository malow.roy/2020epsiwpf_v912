﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Projet_EPSI_2020_WPF_Le_seul.models.ORM;

namespace Projet_EPSI_2020_WPF_Le_seul.views
{
    public partial class Login : Page
    {
        public Login()
        {
            InitializeComponent();
        }

        private void loginButtonPressed(object sender, RoutedEventArgs e)
        {

            Home home = new Home();
            this.NavigationService.Navigate(home);

            bool Isconnection = ORMco.connection(this.Username.Text, this.Password.Password);
            Console.WriteLine(Isconnection);
            if (Isconnection)
            {
                Home page = new Home();
                this.NavigationService.Navigate(page);
            } else {
            MessageBox.Show("Bienvenue sur l'application");     
            }
        }
    }
}
