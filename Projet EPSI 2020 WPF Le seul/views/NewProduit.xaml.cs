﻿using Projet_EPSI_2020_WPF_Le_seul.models.ORM;
using Projet_EPSI_2020_WPF_Le_seul.viewmodels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Projet_EPSI_2020_WPF_Le_seul.views
{
    public partial class NewProduit : Page
    {
        ObservableCollection<LieuViewModel> lieux;
        ObservableCollection<ViewModelCategorie> categories;
        ObservableCollection<ViewModelCo> cos ;
      //  ObservableCollection<ViewModelLot> lots;



        public NewProduit()
        {
            InitializeComponent();

            loadLieux();
            loadCategories();

            loadVendeur();




        }

        private void newProductButtonPressed(object sender, RoutedEventArgs e)
        {
            ViewModelCategorie categorie = (ViewModelCategorie)this.categorieList.SelectedItem;
            LieuViewModel lieu = (LieuViewModel)this.lieuList.SelectedItem;
            ViewModelCo co = (ViewModelCo)this.userList.SelectedItem;
            ORMproduit.AddProduit(ProductName.Text,Description.Text,Volume.Text, Price.Text, Poids.Text, Material.Text,co.Id, categorie.Id, lieu.Id);
            Home page = new Home();
            NavigationService.Navigate(page);
        }

        private void backScreenClick(object sender, RoutedEventArgs e)
        {
            Home page = new Home();
            NavigationService.Navigate(page);
        }

        private void loadCategories()
        {
            categories = ORMcategorie.getAllCategories();
            categorieList.ItemsSource = categories;
        }

        
        private void loadLieux()
        {
            lieux = ORMlieu.getAllLieux();
            lieuList.ItemsSource = lieux;
        }

        
        private void loadVendeur()
        {
            cos = ORMco.getAllCos();
            userList.ItemsSource = cos;
        }


     

    }
}
