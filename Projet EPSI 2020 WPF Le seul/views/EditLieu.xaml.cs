﻿using Projet_EPSI_2020_WPF_Le_seul.models.ORM;
using Projet_EPSI_2020_WPF_Le_seul.viewmodels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Projet_EPSI_2020_WPF_Le_seul.views
{
    public partial class EditLieu : Page
    {

        LieuViewModel editingLieu;

        public EditLieu(LieuViewModel editLieu)
        {
            InitializeComponent();
            loadEditLieu(editLieu);
        }

        private void editPlaceButtonPressed(object sender, RoutedEventArgs e)
        {
            ORMlieu.EditLieu(editingLieu.Id, PlaceName.Text, City.Text, Adress.Text, PostalCode.Text);
            Home page = new Home();
            NavigationService.Navigate(page);
        }

        private void backScreenClick(object sender, RoutedEventArgs e)
        {
            Home page = new Home();
            NavigationService.Navigate(page);
        }

        private void loadEditLieu(LieuViewModel editLieu)
        {
            editingLieu = ORMlieu.getLieu(editLieu.Id);
            PlaceName.Text = editingLieu.Name;
            City.Text = editingLieu.City;
            PostalCode.Text = editingLieu.PostalCode;
            Adress.Text = editingLieu.Address;
        }
    }
}
