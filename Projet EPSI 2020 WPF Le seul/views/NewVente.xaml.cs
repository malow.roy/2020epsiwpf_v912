﻿using Projet_EPSI_2020_WPF_Le_seul.models.ORM;
using Projet_EPSI_2020_WPF_Le_seul.viewmodels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Projet_EPSI_2020_WPF_Le_seul.views
{
    public partial class NewVente : Page
    {

        ObservableCollection<LieuViewModel> lieux;

        public NewVente()
        {
            InitializeComponent();
            loadLieux();
        }

        private void newVenteButtonPressed(object sender, RoutedEventArgs e)
        {
            DateTime dt = (DateTime)Date.SelectedDate;
            LieuViewModel lvm = (LieuViewModel)lieuList.SelectedItem;
            if (no.IsChecked == true){
                ORMvente.AddVente(dt, Titre.Text, false, lvm.Id);
            }
            else if(yes.IsChecked == true){
                ORMvente.AddVente(dt, Titre.Text, true, lvm.Id);
            }
            Home page = new Home();
            NavigationService.Navigate(page);
        }

        private void backScreenClick(object sender, RoutedEventArgs e)
        {
            Home page = new Home();
            NavigationService.Navigate(page);
        }

        private void loadLieux()
        {
            lieux = ORMlieu.getAllLieux();
            lieuList.ItemsSource = lieux;
        }
    }
}
