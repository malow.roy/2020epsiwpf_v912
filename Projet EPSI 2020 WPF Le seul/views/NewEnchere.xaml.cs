﻿using Projet_EPSI_2020_WPF_Le_seul.models.ORM;
using Projet_EPSI_2020_WPF_Le_seul.viewmodels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Projet_EPSI_2020_WPF_Le_seul.views
{

    public partial class NewEnchere : Page
    {

        ObservableCollection<ViewModelVente> ventes;
        ObservableCollection<ViewModelLot> lots;
        ObservableCollection<ViewModelCo> cos;
        public NewEnchere()
        {
            InitializeComponent();

            loadLot();
           loadVente();

            loadAcheteur();
        }

        private void newEnchereButtonPressed(object sender, RoutedEventArgs e)
        {

            ViewModelVente Vente = (ViewModelVente)this.venteList.SelectedItem;
            ViewModelCo Acheteur = (ViewModelCo)this.userList.SelectedItem;
            ViewModelLot Lot = (ViewModelLot)this.lotList.SelectedItem;
            ORMencheres.AddEnchere(Price.Text, Vente.Id, Acheteur.Id, Lot.Id);
            Home page = new Home();
            NavigationService.Navigate(page);
        }

        private void backScreenClick(object sender, RoutedEventArgs e)
        {
            Home page = new Home();
            NavigationService.Navigate(page);
        }

        private void loadVente()
        {
            ventes = ORMvente.getAllVentes();
            venteList.ItemsSource = ventes;
        }

        
        private void loadAcheteur()
        {
            cos = ORMco.getAllCos();
            userList.ItemsSource = cos;
        }


        private void loadLot()
        {
            lots = ORMlot.getAllLots();
            lotList.ItemsSource = lots;
        }

    }
}
