﻿using Projet_EPSI_2020_WPF_Le_seul.models.ORM;
using Projet_EPSI_2020_WPF_Le_seul.viewmodels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Projet_EPSI_2020_WPF_Le_seul.views
{
    public partial class EditLot : Page
    {
        ObservableCollection<ViewModelProduit> produits;
        ViewModelLot editingLot;
        ObservableCollection<ViewModelProduitCheckBox> pList = new ObservableCollection<ViewModelProduitCheckBox>();

        public EditLot(ViewModelLot lot)
        {
            InitializeComponent();
            loadEditLot(lot);
            loadEditProduits(lot);
        }

        private void editLotButtonPressed(object sender, RoutedEventArgs e)
        {
            ObservableCollection<ViewModelProduit> selectedProduits = new ObservableCollection<ViewModelProduit>();

            foreach (ViewModelProduitCheckBox selectedCheckBox in pList)
            {
                if (selectedCheckBox.Valeur == true) selectedProduits.Add(selectedCheckBox.ProduitVM);
            }
            ORMlot.EditLot(editingLot.Id, ProductName.Text, Description.Text, selectedProduits);
            Home page = new Home();
            NavigationService.Navigate(page);
        }

        private void backScreenClick(object sender, RoutedEventArgs e)
        {
            Home page = new Home();
            NavigationService.Navigate(page);
        }

        private void loadEditLot(ViewModelLot editLot)
        {
            editingLot = ORMlot.getLot(editLot.Id);
            ProductName.Text = editingLot.Name;
            Description.Text = editingLot.Description;
        }

        private void loadEditProduits(ViewModelLot lot)
        {
            produits = ORMproduit.getAllProduitsFromLot(lot.Id);
            foreach (ViewModelProduit produit in produits)
            {
                pList.Add(new ViewModelProduitCheckBox(produit.Id, produit, true));
            }
            produits = ORMproduit.getAllProduitsForLot();
            foreach (ViewModelProduit produit in produits)
            {
                pList.Add(new ViewModelProduitCheckBox(produit.Id, produit, false));
            }
            produitsList.ItemsSource = pList;
        }

    }
}
