﻿using Projet_EPSI_2020_WPF_Le_seul.models.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Projet_EPSI_2020_WPF_Le_seul.views
{
    public partial class NewLieu : Page
    {
        public NewLieu()
        {
            InitializeComponent();
        }

        private void newPlaceButtonPressed(object sender, RoutedEventArgs e)
        {
            ORMlieu.AddLieu(PlaceName.Text, City.Text, Adress.Text, PostalCode.Text);
            Home page = new Home();
            NavigationService.Navigate(page);
        }

        private void backScreenClick(object sender, RoutedEventArgs e)
        {
            Home page = new Home();
            NavigationService.Navigate(page);
        }
    }
}
