﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Projet_EPSI_2020_WPF_Le_seul.models.DAL;
using Projet_EPSI_2020_WPF_Le_seul.models.ORM;
using Projet_EPSI_2020_WPF_Le_seul.viewmodels;

namespace Projet_EPSI_2020_WPF_Le_seul.views
{

    public partial class Home : Page
    {
        ObservableCollection<LieuViewModel> lieux;
        ObservableCollection<ViewModelCategorie> categories;
        ObservableCollection<ViewModelVente> ventes;
        ObservableCollection<ViewModelLot> lots;
        ObservableCollection<ViewModelEncheres> encheres;



        /*       int selectedPersonneId;
               ViewModelEncheres myDataObject; // Objet de liaison avec la vue lors de l'ajout d'une personne par exemple.
               ObservableCollection<ViewModelEncheres> en;*/

        public Home()
        {
            InitializeComponent();

            loadLieux();
            loadCategories();
            loadVentes();
            loadLots();

        }

        private void newPlaceButtonClick(object sender, RoutedEventArgs e)
        {
            NewLieu page = new NewLieu();
            NavigationService.Navigate(page);
        }

        private void newEnchereButtonClick(object sender, RoutedEventArgs e)
        {
            NewEnchere page = new NewEnchere();
            NavigationService.Navigate(page);
        }

        private void newVenteButtonClick(object sender, RoutedEventArgs e)
        {
            NewVente page = new NewVente();
            NavigationService.Navigate(page);
        }

        private void newProduitButtonClick(object sender, RoutedEventArgs e)
        {
            NewProduit page = new NewProduit();
            NavigationService.Navigate(page);
        }

        private void newCategorieButtonClick(object sender, RoutedEventArgs e)
        {
            NewCategorie page = new NewCategorie();
            NavigationService.Navigate(page);
        }

        private void newLotButtonClick(object sender, RoutedEventArgs e)
        {
            NewLot page = new NewLot();
            NavigationService.Navigate(page);
        }
        private void editEnchereButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            ViewModelEncheres enchere = button.DataContext as ViewModelEncheres;
            EditEnchere page = new EditEnchere(enchere);
            NavigationService.Navigate(page);
        }
        private void editLieuButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            LieuViewModel lieu = button.DataContext as LieuViewModel;
            EditLieu page = new EditLieu(lieu);
            NavigationService.Navigate(page);
        }

        private void editCategorieButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            ViewModelCategorie categorie = button.DataContext as ViewModelCategorie;
            EditCategorie page = new EditCategorie(categorie);
            NavigationService.Navigate(page);
        }

        private void editVenteButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            ViewModelVente vente = button.DataContext as ViewModelVente;
            EditVente page = new EditVente(vente);
            NavigationService.Navigate(page);
        }

        private void editLotButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            ViewModelLot lot = button.DataContext as ViewModelLot;
            EditLot page = new EditLot(lot);
            NavigationService.Navigate(page);
        }

        private void removeEnchereButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            ViewModelEncheres enchere = button.DataContext as ViewModelEncheres;

            if (MessageBox.Show("Êtes-vous sûr(e) de vouloir supprimer ?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                ORMencheres.RemoveEnchere(enchere.Id);
                Home page = new Home();
                NavigationService.Navigate(page);
            }
            else
            {
                // user clicked no
            }
        }
        private void removeCategorieButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            ViewModelCategorie categorie = button.DataContext as ViewModelCategorie;

            if (MessageBox.Show("Êtes-vous sûr(e) de vouloir supprimer ?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                ORMcategorie.RemoveCategorie(categorie.Id);
                Home page = new Home();
                NavigationService.Navigate(page);
            }
            else
            {
                // user clicked no
            }
        }

        private void removeLieuButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            LieuViewModel lieu = button.DataContext as LieuViewModel;

            if (MessageBox.Show("Êtes-vous sûr(e) de vouloir supprimer ?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                ORMlieu.RemoveLieu(lieu.Id);
                Home page = new Home();
                NavigationService.Navigate(page);
            }
            else
            {
                // user clicked no
            }
        }

        private void removeLotButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            ViewModelLot lot = button.DataContext as ViewModelLot;

            if (MessageBox.Show("Êtes-vous sûr(e) de vouloir supprimer ?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                ORMlot.RemoveLot(lot.Id);
                Home page = new Home();
                NavigationService.Navigate(page);
            }
            else
            {
                // user clicked no
            }
        }

        private void removeVenteButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            ViewModelVente vente = button.DataContext as ViewModelVente;

            if (MessageBox.Show("Êtes-vous sûr(e) de vouloir supprimer ?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                ORMvente.RemoveVente(vente.Id);
                Home page = new Home();
                NavigationService.Navigate(page);
            }
            else
            {
                // user clicked no
            }
        }


        private void loadLieux()
        {
            lieux = ORMlieu.getAllLieux();
            lieuList.ItemsSource = lieux;
        }

        private void loadLots()
        {
            lots = ORMlot.getAllLots();
            for (int i = 0; i<lots.Count; i++)
            {
                lots[i].Products = ORMproduit.getAllProduitsFromLot(lots[i].Id);
            }
            lotList.ItemsSource = lots;
        }
        
        private void loadCategories()
        {
            categories = ORMcategorie.getAllCategories();
            categorieList.ItemsSource = categories;
        }

        private void loadVentes()
        {
            ventes = ORMvente.getAllVentes();
            venteList.ItemsSource = ventes;
        }
    }
}
