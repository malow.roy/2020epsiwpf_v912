﻿using Projet_EPSI_2020_WPF_Le_seul.models.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Projet_EPSI_2020_WPF_Le_seul.views
{
    public partial class NewCategorie : Page
    {
        public NewCategorie()
        {
            InitializeComponent();
        }

        private void newCategorieButtonPressed(object sender, RoutedEventArgs e)
        {
            ORMcategorie.AddCategorie(CategorieName.Text);
            Home page = new Home();
            NavigationService.Navigate(page);
        }

        private void backScreenClick(object sender, RoutedEventArgs e)
        {
            Home page = new Home();
            NavigationService.Navigate(page);

        }
    }
}
