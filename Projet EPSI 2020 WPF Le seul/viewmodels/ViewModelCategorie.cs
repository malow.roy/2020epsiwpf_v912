﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Projet_EPSI_2020_WPF_Le_seul.viewmodels
{
    public class ViewModelCategorie : INotifyPropertyChanged
    {
        private int id;
        private string nom;

        public ViewModelCategorie() { }

        public ViewModelCategorie(int id, string nom)
        {
            this.id = id;
            this.nom = nom;


        }

        public event PropertyChangedEventHandler PropertyChanged;
        public int Id { get { return id; } }


        public string Nom { 
            get { return nom; }
            set
            {
                this.nom = value;
                OnPropertyChanged("nom");
            }
        }

        private void OnPropertyChanged(string info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
                this.PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

    }

}
