﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_EPSI_2020_WPF_Le_seul.viewmodels
{
    public class ViewModelLot : INotifyPropertyChanged
    {
        private int id;
        private string name;
        private string description;
        private ViewModelVente venteVM;
        private ObservableCollection<ViewModelProduit> products;

        public ViewModelLot(int id, string name, string description, ViewModelVente venteVM)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.venteVM = venteVM; 
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public int Id
        {
            get { return id; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged("Name"); }
        }

        public string Description
        {
            get { return description; }
            set { description = value; OnPropertyChanged("Description"); }
        }

        public ViewModelVente VenteVM
        {
            get { return venteVM; }
            set { venteVM = value; OnPropertyChanged("VenteVM"); }
        }

        public ObservableCollection<ViewModelProduit> Products
        {
            get { return products; }
            set { products = value; OnPropertyChanged("Products"); }
        }

        private void OnPropertyChanged(string info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
                this.PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

    }
}
