﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Projet_EPSI_2020_WPF_Le_seul.viewmodels
{
    public class LieuViewModel : INotifyPropertyChanged
    {
        private int id;
        private string name;
        private string city;
        private string address;
        private string postalcode;

        public LieuViewModel()
        {

        }
        public LieuViewModel(int id, string name, string city, string address, string postalcode)
        {
            this.id = id;
            this.name = name;
            this.city = city;
            this.address = address;
            this.postalcode = postalcode;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public int Id
        {
            get { return id; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged("Name"); }
        }

        public string City
        {
            get { return city; }
            set { city = value; OnPropertyChanged("City"); }
        }

        public string Address
        {
            get { return address; }
            set { address = value; OnPropertyChanged("Address"); }
        }

        public string AddressCity
        {
            get { return address + ", " + city; }
            set { address = value; OnPropertyChanged("Address"); }
        }

        public string PostalCode
        {
            get { return postalcode; }
            set { postalcode = value; OnPropertyChanged("Description"); }
        }

        private void OnPropertyChanged(string info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
                this.PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

    }
}
