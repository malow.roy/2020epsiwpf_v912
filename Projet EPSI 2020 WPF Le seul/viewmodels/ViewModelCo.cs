﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_EPSI_2020_WPF_Le_seul.viewmodels
{

    public class ViewModelCo : INotifyPropertyChanged
    {
        private int id;
        private string nom;


        public ViewModelCo()
        {

        }
        public ViewModelCo(int id, string nom)
        {
            this.id = id;
            this.nom = nom;

        }

        public event PropertyChangedEventHandler PropertyChanged;

        public int Id
        {
            get { return id; }
        }

        public string Nom
        {
            get { return nom; }
            set { nom = value; OnPropertyChanged("Name"); }
        }

        private void OnPropertyChanged(string info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
                this.PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

    }
}