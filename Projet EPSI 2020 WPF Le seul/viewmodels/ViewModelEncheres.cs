﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_EPSI_2020_WPF_Le_seul.viewmodels
{
    public class ViewModelEncheres : INotifyPropertyChanged
    {
        private int id;
        private string price;
        private int idVente;
        private int idAcheteur;
        private int idLot;

        public ViewModelEncheres()
        {

        }
        public ViewModelEncheres(int id, string price, int idVente, int idAcheteur, int idLot)
        {
            this.id = id;
            this.price = price;
            this.idVente = idVente;
            this.idAcheteur = idAcheteur;
            this.idLot = idLot;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public int Id
        {
            get { return id; }
        }

        public string Prix
        {
            get { return price; }
            set { price = value; OnPropertyChanged("Price"); }
        }

        public int IdVente
        {
            get { return idVente; }
            set { idVente = value; OnPropertyChanged("IdVente"); }
        }

        public int IdAcheteur
        {
            get { return idAcheteur; }
            set { idAcheteur = value; OnPropertyChanged("IdAcheteur"); }
        }

        public int IdLot
        {
            get { return idLot; }
            set { idLot = value; OnPropertyChanged("IdLot"); }
        }

        private void OnPropertyChanged(string info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
                this.PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

    }
}
