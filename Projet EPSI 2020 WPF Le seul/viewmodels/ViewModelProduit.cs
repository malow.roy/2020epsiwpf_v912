﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Projet_EPSI_2020_WPF_Le_seul.viewmodels
{
    public class ViewModelProduit : INotifyPropertyChanged
    {
        private int id;
        private string titre;
        private string description;
        private string volume;
        private string price;
        private string poids;
        private string materiau;
        private int idVendeur;
        private int idCategorie;
        private int idLot;
        private int idLieu;




        public ViewModelProduit() { }

        public ViewModelProduit(int id, string titre, string description, string volume, string price, string poids, string materiau, int idVendeur, int idCategorie, int idLot, int idLieu)
        {
            this.id = id;
            this.titre = titre;
            this.description = description;
            this.volume = volume;
            this.price = price;
            this.poids = poids;
            this.materiau = materiau;
            this.idVendeur = idVendeur;
            this.idCategorie = idCategorie;
            this.idLot = idLot;
            this.idLieu = idLieu;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public int Id { get { return id; } }


        public string Titre
        {
            get { return titre; }
            set
            {
                this.titre = value;
                OnPropertyChanged("titre");
            }
        }


        public string Description
        {
            get { return description; }
            set
            {
                this.description = value;
                OnPropertyChanged("description");
            }
        }


        public string Volume
        {
            get { return volume; }
            set
            {
                this.volume = value;
                OnPropertyChanged("volume");
            }
        }


        public string Price
        {
            get { return price; }
            set
            {
                this.price = value;
                OnPropertyChanged("price");
            }
        }
        public string Poids
        {
            get { return poids; }
            set
            {
                this.poids = value;
                OnPropertyChanged("poids");
            }
        }
        public string Materiau
        {
            get { return materiau; }
            set
            {
                this.materiau = value;
                OnPropertyChanged("materiau");
            }
        }
        public int IdVendeur
        {
            get { return idVendeur; }
            set
            {
                this.idVendeur = value;
                OnPropertyChanged("idVendeur");
            }
        }
        public int IdCategorie
        {
            get { return idCategorie; }
            set
            {
                this.idCategorie = value;
                OnPropertyChanged("idCategorie");
            }
        }
       
        public int IdLieu
        {
            get { return idLieu; }
            set
            {
                this.idLieu = value;
                OnPropertyChanged("idLieu");
            }
        }

        private void OnPropertyChanged(string info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
                this.PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
