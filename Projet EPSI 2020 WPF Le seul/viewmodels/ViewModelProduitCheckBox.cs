﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_EPSI_2020_WPF_Le_seul.viewmodels
{
    public class ViewModelProduitCheckBox : INotifyPropertyChanged
    {
        private int id;
        private ViewModelProduit produitVM;
        private bool valeur;

        public ViewModelProduitCheckBox(int id, ViewModelProduit produitVM, bool valeur)
        {
            this.id = id;
            this.produitVM = produitVM;
            this.valeur = valeur;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public int Id { get { return id; } }


        public ViewModelProduit ProduitVM
        {
            get { return produitVM; }
            set
            {
                this.produitVM = value;
                OnPropertyChanged("produitVM");
            }
        }


        public bool Valeur
        {
            get { return valeur; }
            set
            {
                this.valeur = value;
                OnPropertyChanged("valeur");
            }
        }

        private void OnPropertyChanged(string info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
                this.PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
