﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Projet_EPSI_2020_WPF_Le_seul.viewmodels
{
    public class ViewModelVente : INotifyPropertyChanged
    {

        private int id;
        private DateTime date;
        private string titre;
        private string isFinished;
        private LieuViewModel lieu;

        public ViewModelVente() { }

        public ViewModelVente(int id, string titre, DateTime date, string Isfinished, LieuViewModel lieu)
        {

            this.id = id;
            this.titre = titre;
            this.date = date;
            this.isFinished = Isfinished;
            this.lieu = lieu;

        }

        public event PropertyChangedEventHandler PropertyChanged;
        public int Id { get { return id; } }


        public string Titre
        {
            get { return titre; }
            set
            {
                this.titre = value;
                OnPropertyChanged("titre");
            }
        }


        public DateTime Date
        {
            get { return date; }
            set
            {
                this.date = value;
                OnPropertyChanged("date");
            }
        }


        public string Isfinished
        {
            get { return isFinished; }
            set
            {
                this.isFinished = value;
                OnPropertyChanged("Isfinished");
            }
        }


        public LieuViewModel IdLieu
        {
            get { return lieu; }
            set
            {
                this.lieu = value;
                OnPropertyChanged("idLieu");
            }
        }

        private void OnPropertyChanged(string info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
                this.PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

    }
}
